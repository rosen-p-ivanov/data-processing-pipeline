package org.demo.reactive.resources;

import org.demo.reactive.domain.Message;
import org.demo.reactive.repository.MongoMessageRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.BodyInserters.fromPublisher;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Profile("message-profile")
@Configuration
public class MessageRouter {
    @Bean
    RouterFunction<?> routes(MongoMessageRepository mr) {
        return route(
                GET("/messages"),
                request -> ok().body(fromPublisher(mr.findAll(), Message.class))
        );
    }
}
