package org.demo.reactive.repository;

import org.demo.reactive.domain.Message;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoMessageRepository extends ReactiveMongoRepository<Message, String> {

}
