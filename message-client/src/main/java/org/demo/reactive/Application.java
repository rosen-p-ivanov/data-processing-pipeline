package org.demo.reactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import reactor.core.Disposable;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class Application implements CommandLineRunner {

    @Autowired
    private ApplicationConfig applicationConfig;

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", "message-profile");
        SpringApplication.run(Application.class, args);
    }


    public void run(String... args) {
        System.out.println("using environment:" + applicationConfig.getEnvironment());
        System.out.println("name:" + applicationConfig.getName());
    }

}
