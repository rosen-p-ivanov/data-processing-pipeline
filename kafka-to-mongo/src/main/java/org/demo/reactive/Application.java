package org.demo.reactive;

import org.demo.reactive.repository.KafkaMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import reactor.core.Disposable;

import javax.annotation.PreDestroy;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class Application implements CommandLineRunner {


    @Autowired
    KafkaMessageRepository kafkaMessageRepository;
    @Autowired
    private ApplicationConfig applicationConfig;

    private Disposable disposable;

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", "message-profile");
        SpringApplication.run(Application.class, args);
    }

    @PreDestroy
    public void destroy() {
        disposable.dispose();
    }

    public void run(String... args) {
        System.out.println("using environment:" + applicationConfig.getEnvironment());
        System.out.println("name:" + applicationConfig.getName());
        System.out.println("kafkaBootstrapServersConfig:" + applicationConfig.getKafkaBootstrapServersConfig());
        disposable = kafkaMessageRepository.consumeMessages(applicationConfig.getKafkaTopic());
    }


}
