package org.demo.reactive.repository;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.demo.reactive.ApplicationConfig;
import org.demo.reactive.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.stereotype.Repository;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOffset;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Repository
public class KafkaMessageRepository {
    private static final Logger logger = LoggerFactory.getLogger(KafkaMessageRepository.class);

    private final ReceiverOptions<String, Message> receiverOptions;
    private final SimpleDateFormat dateFormat;

    private final MongoMessageRepository mmr;

    public KafkaMessageRepository(ApplicationConfig applicationConfig, MongoMessageRepository mmr) {
        this.mmr = mmr;
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, applicationConfig.getKafkaBootstrapServersConfig());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "message-consumer");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, applicationConfig.getKafkaConsummerGroupId());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "org.demo.reactive.domain");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        receiverOptions = ReceiverOptions.create(props);
        dateFormat = new SimpleDateFormat("HH:mm:ss:SSS z dd MMM yyyy");
    }

    public Disposable consumeMessages(String topic) {

        ReceiverOptions<String, Message> options = receiverOptions.subscription(Collections.singleton(topic))
                .addAssignListener(partitions -> logger.debug("onPartitionsAssigned {}", partitions))
                .addRevokeListener(partitions -> logger.debug("onPartitionsRevoked {}", partitions));
        Flux<ReceiverRecord<String, Message>> kafkaFlux = KafkaReceiver.create(options).receive();
        return kafkaFlux
                .doOnError(System.out::println)
                .subscribe(record -> {
                    ReceiverOffset offset = record.receiverOffset();
                    System.out.printf("Received message: topic-partition=%s offset=%d timestamp=%s key=%s value=%s\n",
                            offset.topicPartition(),
                            offset.offset(),
                            dateFormat.format(new Date(record.timestamp())),
                            record.key(),
                            record.value());

                    mmr.save(record.value())
                            .doOnError(error -> logger.error("problem  saving", error))
                            .doOnNext(__ -> offset.acknowledge())
                            .subscribe();


                });
    }
}
