# data-processing-pipeline

## Solution consist of the following services:
  * message-receiver: responsible for receiving json payload (POST )at http://localhost:8080/messages with the message and passing it to Kafka
  * kafka-to-mongo: responsible for pulling the message from Kafka and store it in mongodb
  * message-client: responsible for returning all messages at endpoint: http://localhost:8100/messages

Every project resides in different folder and can be build with ./gradlew clean build.
Default configuration files are stored at resources/application.yaml

1. Build fat jars and corresponding docker images:
```
./build-all-service-image.sh
```
Creates container per every service.

2. Setup external dependencies locally:
2.1 Run kafka docker image:
```
docker run -e RUNTESTS=0 -e RUNNING_SAMPLEDATA=0 -e SAMPLEDATA=0 --rm --net=host landoop/fast-data-dev
```

2.2 create kafka topic:
```
docker run --rm -it --net=host landoop/fast-data-dev kafka-topics --zookeeper localhost:2181 --create --topic message-topic --partitions 1 --replication-factor 1
```

run mongodb docker image:
```
docker run -p 27017:27017 -e ALLOW_EMPTY_PASSWORD=yes bitnami/mongodb:latest
```

3. Use docker-compose to run all services and dependencies into containers:
```
docker-compose up
```
Optionally kafka can be started first (command in 2.1) and commented out from docker-compose.yml

4. Testing.
Example send a message:
curl -X POST http://localhost:8080/messages -H 'Content-Type: application/json' -d '{"sender": "A", "receiver": "B", "content": "just a test"}'
Example receive a message:
curl -X GET http://localhost:8100/messages