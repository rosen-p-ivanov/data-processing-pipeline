package org.demo.reactive.repository;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.demo.reactive.ApplicationConfig;
import org.demo.reactive.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Repository;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOffset;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Repository
public class KafkaMessageRepository {
    private static final Logger logger = LoggerFactory.getLogger(KafkaMessageRepository.class);

    private final KafkaSender<String, Message> sender;
    private final ReceiverOptions<String, Message> receiverOptions;

    public KafkaMessageRepository(ApplicationConfig applicationConfig) {
        Map<String, Object> producerProps = new HashMap<>();
        producerProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, applicationConfig.getKafkaBootstrapServersConfig());
        producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        SenderOptions<String, Message> senderOptions =
                SenderOptions.<String, Message>create(producerProps)
                        .maxInFlight(1024);

        this.sender = KafkaSender.create(senderOptions);

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, applicationConfig.getKafkaBootstrapServersConfig());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "message-consumer");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, applicationConfig.getKafkaConsummerGroupId());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "org.demo.reactive.domain");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        receiverOptions = ReceiverOptions.create(props);
    }

    public Mono<Message> save(Message message) {
        return sender
                .send(
                        Mono.just(
                                SenderRecord
                                        .create("message-topic", null, Instant.now().getEpochSecond(),
                                                message.getId().toString(), message, 1)
                        )
                )
                .next()
                .doOnError(e -> logger.error("Send failed", e))
                .flatMap(result -> {
                    if (result.exception() != null) {
                        logger.error("Unable to send message", result.exception());
                        return Mono.empty();
                    } else {
                        return Mono.just(message);
                    }
                });
    }

    public Disposable consumeMessages(String topic, CountDownLatch latch) {

        ReceiverOptions<String, Message> options = receiverOptions.subscription(Collections.singleton(topic))
                .addAssignListener(partitions -> logger.debug("onPartitionsAssigned {}", partitions))
                .addRevokeListener(partitions -> logger.debug("onPartitionsRevoked {}", partitions));
        Flux<ReceiverRecord<String, Message>> kafkaFlux = KafkaReceiver.create(options).receive();
        return kafkaFlux
                .doOnError(System.out::println)
                .subscribe(record -> {
                    ReceiverOffset offset = record.receiverOffset();
                    offset.acknowledge();
                    latch.countDown();
                });
    }
}
