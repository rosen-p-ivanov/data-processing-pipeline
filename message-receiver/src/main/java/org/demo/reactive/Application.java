package org.demo.reactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private ApplicationConfig applicationConfig;

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", "message-profile");
        SpringApplication.run(Application.class, args);
    }

    public void run(String... args) {
        System.out.println("using environment:" + applicationConfig.getEnvironment());
        System.out.println("name:" + applicationConfig.getName());
        System.out.println("kafkaBootstrapServersConfig:" + applicationConfig.getKafkaBootstrapServersConfig());
    }

}
