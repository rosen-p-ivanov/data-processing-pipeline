package org.demo.reactive.resources;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Profile("message-profile")
@Configuration
public class MessageRouter {
    @Bean
    RouterFunction<ServerResponse> routes(MessageHandler messageHandler) {
        return route(
                POST("/messages")
                        .and(contentType(MediaType.APPLICATION_JSON)),
                messageHandler::postMessage);
    }
}
