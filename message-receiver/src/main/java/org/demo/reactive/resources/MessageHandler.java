package org.demo.reactive.resources;

import org.demo.reactive.domain.Message;
import org.demo.reactive.repository.KafkaMessageRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.web.reactive.function.BodyInserters.fromPublisher;

@Component
public class MessageHandler {
    private final KafkaMessageRepository repository;

    public MessageHandler(KafkaMessageRepository repository) {
        this.repository = repository;
    }


    public Mono<ServerResponse> postMessage(ServerRequest request) {
        final Mono<Message> messageMono = request.bodyToMono(Message.class);
        final UUID id = UUID.randomUUID();
        return messageMono
                .map(message -> repository.save(new Message(id, message)))
                .flatMap(messageSaved ->
                        ServerResponse.created(
                                UriComponentsBuilder.fromPath("messages/" + id)
                                        .build()
                                        .toUri())
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(fromPublisher(messageSaved, Message.class)))
                .switchIfEmpty(ServerResponse.status(500).build());
    }
}
