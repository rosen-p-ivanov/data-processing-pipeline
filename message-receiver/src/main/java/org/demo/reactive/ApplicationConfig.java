package org.demo.reactive;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class ApplicationConfig {
    private String name;
    private String environment;
    private String kafkaBootstrapServersConfig;
    private String kafkaTopic;
    private String kafkaConsummerGroupId;

    public String getKafkaConsummerGroupId() {
        return kafkaConsummerGroupId;
    }

    public void setKafkaConsummerGroupId(String kafkaConsummerGroupId) {
        this.kafkaConsummerGroupId = kafkaConsummerGroupId;
    }

    public String getKafkaTopic() {
        return kafkaTopic;
    }

    public void setKafkaTopic(String kafkaTopic) {
        this.kafkaTopic = kafkaTopic;
    }

    public String getKafkaBootstrapServersConfig() {
        return kafkaBootstrapServersConfig;
    }

    public void setKafkaBootstrapServersConfig(String kafkaBootstrapServersConfig) {
        this.kafkaBootstrapServersConfig = kafkaBootstrapServersConfig;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
}
